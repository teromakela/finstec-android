package fi.finstec.finstec.model;

/**
 * Created by walter on 9/4/16.
 */

import android.content.Context;

public class DatabaseFactory {

    private DatabaseHelper database;

    private ClientsDataSource clientsDataSource;
    private SitesDataSource sitesDataSource;
    private EquipmentsDataSource equipmentsDataSource;
    private MaintenancesDataSource maintenancesDataSource;

    private Context context;

    private static DatabaseFactory instance;

    private DatabaseFactory(Context context) {
        database = new DatabaseHelper(context);
        this.context = context;
    }

    public static DatabaseFactory getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseFactory(context);
        }
        return instance;
    }

    public synchronized ClientsDataSource getClientsDataSource() {
        if (clientsDataSource == null) {
            clientsDataSource = new ClientsDataSource(database);
        }
        return clientsDataSource;
    }

    public synchronized SitesDataSource getSitesDataSource() {
        if (sitesDataSource == null) {
            sitesDataSource = new SitesDataSource(database);
        }
        return sitesDataSource;
    }

    public synchronized EquipmentsDataSource getEquipmentsDataSource() {
        if (equipmentsDataSource == null) {
            equipmentsDataSource = new EquipmentsDataSource(database);
        }
        return equipmentsDataSource;
    }

    public synchronized MaintenancesDataSource getMaintenancesDataSource() {
        if (maintenancesDataSource == null) {
            maintenancesDataSource = new MaintenancesDataSource(database);
        }
        return maintenancesDataSource;
    }


}

