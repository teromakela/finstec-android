package fi.finstec.finstec.model;

/**
 * Created by walter on 9/4/16.
 */
public class ClientEntry {

    private long databaseId;
    private String name;

    public long getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(long databaseId) {
        this.databaseId = databaseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
