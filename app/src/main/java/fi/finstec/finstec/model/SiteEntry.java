package fi.finstec.finstec.model;

/**
 * Created by walter on 9/4/16.
 */
public class SiteEntry {
    private long databaseId;
    private ClientEntry client;
    private String name;

    public long getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(long databaseId) {
        this.databaseId = databaseId;
    }

    public ClientEntry getClient() {
        return client;
    }

    public void setClient(ClientEntry client) {
        this.client = client;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
