package fi.finstec.finstec.model;


import java.util.Date;
import java.util.HashMap;

/**
 * Created by walter on 9/5/16.
 */
public class EquipmentEntry {

    public enum EquipmentTypes {
        AUTOCLAVE("autoclave"),
        WASHING_MACHINE("washing_machine"),
        WATER_FILTRATION_SYSTEM("water_filtration_system");

        private final String type;


        private EquipmentTypes(final String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return type;
        }

        public static EquipmentTypes fromString(String type) {
            for (EquipmentTypes b : EquipmentTypes.values()) {
                if (b.toString().equalsIgnoreCase(type)) {
                    return b;
                }
            }
            return null;
        }
    };

    private long databaseId;
    private SiteEntry site;
    private String make;
    private String model;
    private String serialNumber;
    private Date usedSince;
    private HashMap<String, Integer> maintenanceInterval;
    private EquipmentTypes equipmentType;
    private String localName;

    public long getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(long databaseId) {
        this.databaseId = databaseId;
    }

    public SiteEntry getSite() {
        return site;
    }

    public void setSite(SiteEntry site) {
        this.site = site;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Date getUsedSince() {
        return usedSince;
    }

    public void setUsedSince(Date usedSince) {
        this.usedSince = usedSince;
    }

    public HashMap<String, Integer> getMaintenanceInterval() {
        return maintenanceInterval;
    }

    public void setMaintenanceInterval(HashMap<String, Integer> maintenanceInterval) {
        this.maintenanceInterval = maintenanceInterval;
    }

    public EquipmentTypes getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(EquipmentTypes equipmentType) {
        this.equipmentType = equipmentType;
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }
}
