package fi.finstec.finstec.model;

import java.util.Date;

/**
 * Created by walter on 9/9/16.
 */
public class MaintenanceEntry {

    public enum OperationTypes {
        SCHEDULED_MAINTENANCE("scheduled_maintenance"),
        CHECKUP_MAINTENANCE("checkup_maintenance"),
        VALIDATION_MAINTENANCE("validation_maintenance"),
        PRESSURE_CHECK_MAINTENANCE("pressure_check_maintenance");

        private final String type;

        private OperationTypes(final String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return type;
        }

        public static OperationTypes fromString(String type) {
            for (OperationTypes b : OperationTypes.values()) {
                if (b.toString().equalsIgnoreCase(type)) {
                    return b;
                }
            }
            return null;
        }
    };

    private long databaseId;
    private EquipmentEntry equipment;
    private OperationTypes operationType;
    private Date date;
    private String comment;
    private String servicePerson;
    private Date nextMaintenance;
    private String photoPath;
    private byte[] photoThumbnail;

    public long getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(long databaseId) {
        this.databaseId = databaseId;
    }

    public EquipmentEntry getEquipment() {
        return equipment;
    }

    public void setEquipment(EquipmentEntry equipment) {
        this.equipment = equipment;
    }

    public OperationTypes getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationTypes operationType) {
        this.operationType = operationType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getServicePerson() {
        return servicePerson;
    }

    public void setServicePerson(String servicePerson) {
        this.servicePerson = servicePerson;
    }

    public Date getNextMaintenance() {
        return nextMaintenance;
    }

    public void setNextMaintenance(Date nextMaintenance) {
        this.nextMaintenance = nextMaintenance;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public byte[] getPhotoThumbnail() {
        return photoThumbnail;
    }

    public void setPhotoThumbnail(byte[] photoThumbnail) {
        this.photoThumbnail = photoThumbnail;
    }
}
