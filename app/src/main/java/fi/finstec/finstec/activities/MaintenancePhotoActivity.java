package fi.finstec.finstec.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import java.io.IOException;

import fi.finstec.finstec.R;
import fi.finstec.finstec.model.DatabaseFactory;
import fi.finstec.finstec.model.EquipmentEntry;
import fi.finstec.finstec.model.MaintenanceEntry;
import fi.finstec.finstec.model.MaintenancesDataSource;
import fi.finstec.finstec.uihelpers.MenuActivity;
import fi.finstec.finstec.utils.BitmapUtils;

public class MaintenancePhotoActivity extends MenuActivity {
    private static final String TAG = MaintenanceListActivity.class.getSimpleName();

    public static final String PHOTO_PATH_EXTRA = "photoPathExtra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintenance_photo);

        String defValue= "";
        String photoPath = getIntent().getExtras().getString(PHOTO_PATH_EXTRA, defValue);

        setupToolbar();

        ImageView photoViewImage = (ImageView)findViewById(R.id.photoImageView);

        Bitmap bitmap = BitmapUtils.getRotatedBitmap(MaintenancePhotoActivity.this, photoPath);
        if (bitmap == null) {
            Log.e(TAG, "Error reading image: " + photoPath);
            return;
        }
        // Set photo
        photoViewImage.setImageBitmap(bitmap);

    }

    private void setupToolbar() {
        android.widget.Toolbar toolbar = (android.widget.Toolbar)findViewById(R.id.toolbar);
        setActionBar(toolbar);
        setupToolbarMenu(toolbar);

        // Set title
        TextView title = (TextView)toolbar.findViewById(R.id.titleTextView);
        title.setText("Picture");

        ImageButton backButton = (ImageButton)toolbar.findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SearchView searchView = (SearchView)toolbar.findViewById(R.id.searchView);
        searchView.setVisibility(View.GONE);
    }
}
