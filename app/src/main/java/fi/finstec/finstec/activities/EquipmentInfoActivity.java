package fi.finstec.finstec.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import fi.finstec.finstec.R;
import fi.finstec.finstec.model.DatabaseFactory;
import fi.finstec.finstec.model.EquipmentEntry;
import fi.finstec.finstec.model.MaintenanceEntry;
import fi.finstec.finstec.model.MaintenancesDataSource;
import fi.finstec.finstec.uihelpers.MenuActivity;
import fi.finstec.finstec.utils.BitmapUtils;
import fi.finstec.finstec.utils.DateUtils;

public class EquipmentInfoActivity extends MenuActivity {
    private static final String TAG = EquipmentsActivity.class.getSimpleName();

    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;
    private EquipmentEntry equipmentEntry;

    public static final String EQUIPMENT_EXTRA = "equipmentExtra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipment_info);


        String defValue= new Gson().toJson(new EquipmentEntry());
        String jsonString = getIntent().getExtras().getString(EQUIPMENT_EXTRA, defValue);
        equipmentEntry = new Gson().fromJson(jsonString, EquipmentEntry.class);

        Log.d(TAG, "onCreate eqId: " + equipmentEntry.getDatabaseId() + " serial: " + equipmentEntry.getSerialNumber());

        setupToolbar();

        ////////
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new RecyclerAdapter(this);
        recyclerView.setAdapter(adapter);

        setupNewMaintenanceButton();

    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();

        adapter.notifyDataSetChanged();
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setupToolbarMenu(toolbar);

        // Back button
        Drawable back = getResources().getDrawable(R.drawable.ic_arrow_back, null);
        if (back != null) {
            back.setTint(ContextCompat.getColor(this, android.R.color.white));
            toolbar.setNavigationIcon(back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }

        // Set the title bar title
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout)findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(equipmentEntry.getSerialNumber());

        TextView titlebarExpandedTopText = (TextView)findViewById(R.id.titlebarExpandedTopText);
        String equipmentType = equipmentEntry.getEquipmentType().toString();
        String localizedType = getString(getResources().getIdentifier(equipmentType, "string", getPackageName()));
        titlebarExpandedTopText.setText(localizedType);

        TextView titlebarExpandedBottomText = (TextView)findViewById(R.id.titlebarExpandedBottomText);
        String bottomText = equipmentEntry.getMake() + " " + equipmentEntry.getModel();
        titlebarExpandedBottomText.setText(bottomText);

        // Set the parallax image
        ImageView toolbarImage = (ImageView)findViewById(R.id.toolbarImage);
        toolbarImage.setImageResource(BitmapUtils.getToolbarImage(equipmentEntry.getMake(), equipmentEntry.getModel()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    private void setupNewMaintenanceButton() {
        View newMaintenanceButton = findViewById(R.id.newMaintenanceButton);
        newMaintenanceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "newMaintenanceButton clicked");

                Intent intent = new Intent(EquipmentInfoActivity.this, NewMaintenanceActivity.class);
                String jsonString = new Gson().toJson(equipmentEntry);
                intent.putExtra(NewMaintenanceActivity.EQUIPMENT_EXTRA, jsonString);

                startActivity(intent);
            }
        });
    }

    private class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

        private Activity activity;

        public RecyclerAdapter(Activity activity) {
            this.activity = activity;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

            //inflate your layout and pass it to view holder
            LayoutInflater inflater = activity.getLayoutInflater();
            View view = inflater.inflate(R.layout.equipment_info_details, viewGroup, false);

            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerAdapter.ViewHolder viewHolder, int position) {

            View view = viewHolder.itemView;

            // Client
            TextView clientTextView = (TextView)view.findViewById(R.id.clientTextView);
            clientTextView.setText(equipmentEntry.getSite().getClient().getName());

            // Site
            TextView siteTextView = (TextView)view.findViewById(R.id.siteTextView);
            siteTextView.setText(equipmentEntry.getSite().getName());

            // Local name
            TextView localNameTextView = (TextView)view.findViewById(R.id.localNameTextView);
            localNameTextView.setText(equipmentEntry.getLocalName());

            // Serial number
            TextView serialNumberTextView = (TextView)view.findViewById(R.id.serialNumberTextView);
            serialNumberTextView.setText(equipmentEntry.getSerialNumber());

            // UsedSince
            TextView usedSinceTextView = (TextView)view.findViewById(R.id.usedSinceTextView);
            usedSinceTextView.setText(DateUtils.dateToDisplayString(equipmentEntry.getUsedSince()));

            // Last Maintenance
            MaintenancesDataSource maintenancesDataSource = DatabaseFactory.getInstance(EquipmentInfoActivity.this).getMaintenancesDataSource();
            TextView lastMaintenanceTextView = (TextView)view.findViewById(R.id.lastMaintenanceTextView);
            TextView lastMaintenanceTypeTextView = (TextView)view.findViewById(R.id.lastMaintenanceTypeTextView);

            MaintenanceEntry lastMaintenanceEntry = maintenancesDataSource.getLastMaintenancesByEquipment(equipmentEntry);
            if (lastMaintenanceEntry != null) {
                lastMaintenanceTextView.setText(DateUtils.dateToDisplayString(lastMaintenanceEntry.getDate()));

                String localizedType = getString(getResources().getIdentifier(lastMaintenanceEntry.getOperationType().toString(), "string", getPackageName()));
                lastMaintenanceTypeTextView.setText(localizedType);
            }
            else {
                lastMaintenanceTextView.setText("-");
                lastMaintenanceTypeTextView.setText("-");
            }

            // Next Maintenance
            MaintenanceEntry nextMaintenanceEntry = maintenancesDataSource.getNextMaintenancesByEquipment(equipmentEntry);

            TextView nextMaintenanceTextView = (TextView)view.findViewById(R.id.nextMaintenanceTextView);
            TextView nextMaintenanceTypeTextView = (TextView)view.findViewById(R.id.nextMaintenanceTypeTextView);
            if (nextMaintenanceEntry != null) {
                nextMaintenanceTextView.setText(DateUtils.dateToDisplayString(nextMaintenanceEntry.getNextMaintenance()));

                String localizedType = getString(getResources().getIdentifier(nextMaintenanceEntry.getOperationType().toString(), "string", getPackageName()));
                nextMaintenanceTypeTextView.setText(localizedType);
            }
            else {
                nextMaintenanceTextView.setText("-");
                nextMaintenanceTypeTextView.setText("-");
            }

            setupMaintenanceListButton(view);
        }

        @Override
        public int getItemCount() {
            return 1;
        }

        /**
         * View holder to display each RecylerView item
         */
        protected class ViewHolder extends RecyclerView.ViewHolder {

            public ViewHolder(View view) {
                super(view);
            }
        }

        private void setupMaintenanceListButton(View view) {
            View maintenanceHistoryButton = view.findViewById(R.id.maintenanceHistoryButton);
            maintenanceHistoryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "maintenanceHistoryButton onClick");

                    Intent intent = new Intent(EquipmentInfoActivity.this, MaintenanceListActivity.class);
                    String jsonString = new Gson().toJson(equipmentEntry);
                    intent.putExtra(MaintenanceListActivity.EQUIPMENT_EXTRA, jsonString);

                    startActivity(intent);
                }
            });
        }
    }

}

