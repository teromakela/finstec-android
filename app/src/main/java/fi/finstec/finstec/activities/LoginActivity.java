package fi.finstec.finstec.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import fi.finstec.finstec.R;
import fi.finstec.finstec.core.AppSettings;
import fi.finstec.finstec.model.ClientEntry;
import fi.finstec.finstec.model.ClientsDataSource;
import fi.finstec.finstec.model.DatabaseFactory;
import fi.finstec.finstec.model.EquipmentEntry;
import fi.finstec.finstec.model.EquipmentsDataSource;
import fi.finstec.finstec.model.MaintenanceEntry;
import fi.finstec.finstec.model.SiteEntry;
import fi.finstec.finstec.model.SitesDataSource;
import fi.finstec.finstec.utils.AlertUtils;

public class LoginActivity extends Activity {
    private final static String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        populateDatabaseWithFakeData();

        final AppSettings appSettings = AppSettings.getInstance(this);

        // Autofill username if available
        final TextView userNameTextView = (TextView)findViewById(R.id.userNameEditText);
        if (appSettings.getUserName().length() > 0) {
            userNameTextView.setText(appSettings.getUserName());
        }

        Button loginButton = (Button)findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "loginButton onClick");

                final String userName = userNameTextView.getText().toString();
                final String password = ((TextView)findViewById(R.id.passwordEditText)).getText().toString();

                if (userName.trim().length() == 0 || password.trim().length() == 0) {
                    AlertUtils.showErrorNotification(LoginActivity.this, getString(R.string.empty_user_or_password));
                }
                else {
                    // Perform the login, if ok

                    appSettings.setUserName(userName);
                    appSettings.setUserToken("token");

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                }

            }
        });

        // Check if user has token and move to main view
        if (appSettings.getUserToken().length() > 0) {

            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    private void populateDatabaseWithFakeData() {
        DatabaseFactory databaseFactory = DatabaseFactory.getInstance(this);

        ClientsDataSource clientsDataSource = databaseFactory.getClientsDataSource();
        SitesDataSource sitesDataSource = databaseFactory.getSitesDataSource();
        EquipmentsDataSource equipmentsDataSource = databaseFactory.getEquipmentsDataSource();

        // Only put data there if there is no data
        if (clientsDataSource.getAllClients().size() == 0) {
            // helper date variables
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String nowDateString = "13-09-2016";
            String futureDateString = "13-03-2017";
            Date nowDate = null;
            Date futureDate = null;
            try {
                nowDate = sdf.parse(nowDateString);
                futureDate = sdf.parse(futureDateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            // Add the clients
            ClientEntry yliopisto = new ClientEntry();
            yliopisto.setName("Yliopisto");
            yliopisto = clientsDataSource.addClient(yliopisto);
            assert yliopisto != null;

            ClientEntry keskussairaala = new ClientEntry();
            keskussairaala.setName("Keskussairaala");
            keskussairaala = clientsDataSource.addClient(keskussairaala);
            assert keskussairaala != null;

            ClientEntry terveyskeskus = new ClientEntry();
            terveyskeskus.setName("Terveyskeskus");
            terveyskeskus = clientsDataSource.addClient(terveyskeskus);
            assert terveyskeskus != null;


            // Add sites
            SiteEntry yliopistoSite = new SiteEntry();
            yliopistoSite.setName("Meilahti");
            yliopistoSite.setClient(yliopisto);
            yliopistoSite = sitesDataSource.addSite(yliopistoSite);
            assert yliopistoSite != null;

            SiteEntry emptySite = new SiteEntry();
            emptySite.setName("Viikki");
            emptySite.setClient(yliopisto);
            emptySite = sitesDataSource.addSite(emptySite);
            assert emptySite != null;

            SiteEntry keskussairaalaSite = new SiteEntry();
            keskussairaalaSite.setName("Töölö");
            keskussairaalaSite.setClient(keskussairaala);
            keskussairaalaSite = sitesDataSource.addSite(keskussairaalaSite);
            assert keskussairaalaSite != null;

            SiteEntry terveyskeskusSite = new SiteEntry();
            terveyskeskusSite.setName("Aurora");
            terveyskeskusSite.setClient(terveyskeskus);
            terveyskeskusSite = sitesDataSource.addSite(terveyskeskusSite);
            assert terveyskeskusSite != null;

            // Add equipments

            // yliopisto equipments
            EquipmentEntry equipmentEntry1 = new EquipmentEntry();
            equipmentEntry1.setSite(yliopistoSite);
            equipmentEntry1.setMake("Celitron");
            equipmentEntry1.setModel("ISS-575");
            equipmentEntry1.setSerialNumber("321862331");
            equipmentEntry1.setEquipmentType(EquipmentEntry.EquipmentTypes.AUTOCLAVE);
            equipmentEntry1.setUsedSince(new Date());
            HashMap<String, Integer> maintenanceInterval1 = new HashMap<String, Integer>();
            maintenanceInterval1.put(MaintenanceEntry.OperationTypes.CHECKUP_MAINTENANCE.toString(), 1);
            maintenanceInterval1.put(MaintenanceEntry.OperationTypes.PRESSURE_CHECK_MAINTENANCE.toString(), 2);
            maintenanceInterval1.put(MaintenanceEntry.OperationTypes.SCHEDULED_MAINTENANCE.toString(), 3);
            maintenanceInterval1.put(MaintenanceEntry.OperationTypes.VALIDATION_MAINTENANCE.toString(), 4);
            equipmentEntry1.setMaintenanceInterval(maintenanceInterval1);
            equipmentEntry1.setLocalName("Länsisiiven autoklaavi");

            equipmentEntry1 = equipmentsDataSource.addEquipment(equipmentEntry1);
            assert equipmentEntry1 != null;

            EquipmentEntry equipmentEntry2 = new EquipmentEntry();
            equipmentEntry2.setSite(yliopistoSite);
            equipmentEntry2.setMake("Finn-Aqua");
            equipmentEntry2.setModel("A-669 D");
            equipmentEntry2.setSerialNumber("32188231");
            equipmentEntry2.setEquipmentType(EquipmentEntry.EquipmentTypes.AUTOCLAVE);
            equipmentEntry2.setUsedSince(new Date());
            HashMap<String, Integer> maintenanceInterval2 = new HashMap<String, Integer>();
            maintenanceInterval2.put(MaintenanceEntry.OperationTypes.CHECKUP_MAINTENANCE.toString(), 1);
            maintenanceInterval2.put(MaintenanceEntry.OperationTypes.PRESSURE_CHECK_MAINTENANCE.toString(), 2);
            maintenanceInterval2.put(MaintenanceEntry.OperationTypes.SCHEDULED_MAINTENANCE.toString(), 3);
            maintenanceInterval2.put(MaintenanceEntry.OperationTypes.VALIDATION_MAINTENANCE.toString(), 4);
            equipmentEntry2.setMaintenanceInterval(maintenanceInterval2);
            equipmentEntry2.setLocalName("Kellarilaboratorion autoklaavi");

            equipmentEntry2 = equipmentsDataSource.addEquipment(equipmentEntry2);
            assert equipmentEntry2 != null;

            EquipmentEntry equipmentEntry3 = new EquipmentEntry();
            equipmentEntry3.setSite(yliopistoSite);
            equipmentEntry3.setMake("AT-OS");
            equipmentEntry3.setModel("AWD-655-10");
            equipmentEntry3.setSerialNumber("004312");
            equipmentEntry3.setEquipmentType(EquipmentEntry.EquipmentTypes.WASHING_MACHINE);
            equipmentEntry3.setUsedSince(new Date());
            HashMap<String, Integer> maintenanceInterval3 = new HashMap<String, Integer>();
            maintenanceInterval3.put(MaintenanceEntry.OperationTypes.CHECKUP_MAINTENANCE.toString(), 1);
            maintenanceInterval3.put(MaintenanceEntry.OperationTypes.PRESSURE_CHECK_MAINTENANCE.toString(), 2);
            maintenanceInterval3.put(MaintenanceEntry.OperationTypes.SCHEDULED_MAINTENANCE.toString(), 3);
            maintenanceInterval3.put(MaintenanceEntry.OperationTypes.VALIDATION_MAINTENANCE.toString(), 4);
            equipmentEntry3.setMaintenanceInterval(maintenanceInterval3);
            equipmentEntry3.setLocalName("Länsisiiven pesukone");

            equipmentEntry3 = equipmentsDataSource.addEquipment(equipmentEntry3);
            assert equipmentEntry3 != null;

            // Keskussairaala equipments
            EquipmentEntry equipmentEntry4 = new EquipmentEntry();
            equipmentEntry4.setSite(keskussairaalaSite);
            equipmentEntry4.setMake("Celitron");
            equipmentEntry4.setModel("A-456");
            equipmentEntry4.setSerialNumber("321765300");
            equipmentEntry4.setEquipmentType(EquipmentEntry.EquipmentTypes.AUTOCLAVE);
            equipmentEntry4.setUsedSince(new Date());
            HashMap<String, Integer> maintenanceInterval4 = new HashMap<String, Integer>();
            maintenanceInterval4.put(MaintenanceEntry.OperationTypes.CHECKUP_MAINTENANCE.toString(), 1);
            maintenanceInterval4.put(MaintenanceEntry.OperationTypes.PRESSURE_CHECK_MAINTENANCE.toString(), 2);
            maintenanceInterval4.put(MaintenanceEntry.OperationTypes.SCHEDULED_MAINTENANCE.toString(), 3);
            maintenanceInterval4.put(MaintenanceEntry.OperationTypes.VALIDATION_MAINTENANCE.toString(), 4);
            equipmentEntry4.setMaintenanceInterval(maintenanceInterval4);
            equipmentEntry4.setLocalName("Keskussairaalan autoklaavi");

            equipmentEntry4 = equipmentsDataSource.addEquipment(equipmentEntry4);
            assert equipmentEntry4 != null;

            EquipmentEntry equipmentEntry5 = new EquipmentEntry();
            equipmentEntry5.setSite(keskussairaalaSite);
            equipmentEntry5.setMake("Purecleer");
            equipmentEntry5.setModel("RO-3500");
            equipmentEntry5.setSerialNumber("00620");
            equipmentEntry5.setEquipmentType(EquipmentEntry.EquipmentTypes.WATER_FILTRATION_SYSTEM);
            equipmentEntry5.setUsedSince(new Date());
            HashMap<String, Integer> maintenanceInterval5 = new HashMap<String, Integer>();
            maintenanceInterval5.put(MaintenanceEntry.OperationTypes.CHECKUP_MAINTENANCE.toString(), 1);
            maintenanceInterval5.put(MaintenanceEntry.OperationTypes.PRESSURE_CHECK_MAINTENANCE.toString(), 2);
            maintenanceInterval5.put(MaintenanceEntry.OperationTypes.SCHEDULED_MAINTENANCE.toString(), 3);
            maintenanceInterval5.put(MaintenanceEntry.OperationTypes.VALIDATION_MAINTENANCE.toString(), 4);
            equipmentEntry5.setMaintenanceInterval(maintenanceInterval5);
            equipmentEntry5.setLocalName("Taukotilan vesikone");

            equipmentEntry5 = equipmentsDataSource.addEquipment(equipmentEntry5);
            assert equipmentEntry5 != null;

            // Terveyskeskus equipments
            EquipmentEntry equipmentEntry6 = new EquipmentEntry();
            equipmentEntry6.setSite(terveyskeskusSite);
            equipmentEntry6.setMake("Celitron");
            equipmentEntry6.setModel("AC-450");
            equipmentEntry6.setSerialNumber("32133853");
            equipmentEntry6.setEquipmentType(EquipmentEntry.EquipmentTypes.AUTOCLAVE);
            equipmentEntry6.setUsedSince(new Date());
            HashMap<String, Integer> maintenanceInterval6 = new HashMap<String, Integer>();
            maintenanceInterval6.put(MaintenanceEntry.OperationTypes.CHECKUP_MAINTENANCE.toString(), 1);
            maintenanceInterval6.put(MaintenanceEntry.OperationTypes.PRESSURE_CHECK_MAINTENANCE.toString(), 2);
            maintenanceInterval6.put(MaintenanceEntry.OperationTypes.SCHEDULED_MAINTENANCE.toString(), 3);
            maintenanceInterval6.put(MaintenanceEntry.OperationTypes.VALIDATION_MAINTENANCE.toString(), 4);
            equipmentEntry6.setMaintenanceInterval(maintenanceInterval6);
            equipmentEntry6.setLocalName("Autoklaavi labrassa");

            equipmentEntry6 = equipmentsDataSource.addEquipment(equipmentEntry6);
            assert equipmentEntry6 != null;

            EquipmentEntry equipmentEntry7 = new EquipmentEntry();
            equipmentEntry7.setSite(terveyskeskusSite);
            equipmentEntry7.setMake("AT-OS");
            equipmentEntry7.setModel("AF65-EW");
            equipmentEntry7.setSerialNumber("0032632");
            equipmentEntry7.setEquipmentType(EquipmentEntry.EquipmentTypes.WASHING_MACHINE);
            equipmentEntry7.setUsedSince(new Date());
            HashMap<String, Integer> maintenanceInterval7 = new HashMap<String, Integer>();
            maintenanceInterval7.put(MaintenanceEntry.OperationTypes.CHECKUP_MAINTENANCE.toString(), 1);
            maintenanceInterval7.put(MaintenanceEntry.OperationTypes.PRESSURE_CHECK_MAINTENANCE.toString(), 2);
            maintenanceInterval7.put(MaintenanceEntry.OperationTypes.SCHEDULED_MAINTENANCE.toString(), 3);
            maintenanceInterval7.put(MaintenanceEntry.OperationTypes.VALIDATION_MAINTENANCE.toString(), 4);
            equipmentEntry7.setMaintenanceInterval(maintenanceInterval7);
            equipmentEntry7.setLocalName("Terveyskeskuksen pesukone");

            equipmentEntry7 = equipmentsDataSource.addEquipment(equipmentEntry7);
            assert equipmentEntry7 != null;

            Log.d(TAG, "Added dummy database");
        }
    }
}
