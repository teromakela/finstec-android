package fi.finstec.finstec.activities;

import android.Manifest;
import android.app.Activity;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.google.gson.Gson;

import java.util.List;

import fi.finstec.finstec.R;
import fi.finstec.finstec.model.DatabaseFactory;
import fi.finstec.finstec.model.EquipmentEntry;
import fi.finstec.finstec.model.EquipmentsDataSource;
import fi.finstec.finstec.uihelpers.MenuActivity;
import fi.finstec.finstec.utils.AlertUtils;

public class MainActivity extends MenuActivity {
    private final static String TAG = MainActivity.class.getSimpleName();

    private SearchView searchView;
    private SuggestionsAdapter suggestionsAdapter;
    private List<EquipmentEntry> suggestionEquipmentEntries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupToolbar();

        setupQrCodeButton();
        setupEquipmentRegisterButton();
    }

    @Override
    protected void onResume() {
        super.onResume();

        searchView.onActionViewCollapsed();
    }

    @Override
    public void onBackPressed() {
        // Close the app
        finishAffinity();
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setActionBar(toolbar);
        setupToolbarMenu(toolbar);

        ImageButton backButton = (ImageButton)toolbar.findViewById(R.id.backButton);
        backButton.setVisibility(View.GONE);

        // Setup searchview
        searchView = (SearchView)toolbar.findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d(TAG, "onQueryTextSubmit: " + query);

                EquipmentsDataSource equipmentsDataSource = DatabaseFactory.getInstance(MainActivity.this).getEquipmentsDataSource();
                EquipmentEntry equipmentEntry = equipmentsDataSource.getEquipmentBySerialNumber(query);

                if (equipmentEntry != null) {
                    Intent intent = new Intent(MainActivity.this, EquipmentInfoActivity.class);
                    String jsonString = new Gson().toJson(equipmentEntry);
                    intent.putExtra(EquipmentInfoActivity.EQUIPMENT_EXTRA, jsonString);

                    startActivity(intent);
                }
                else {

                    String message = String.format(getString(R.string.cant_find_equipment_with_id), query);
                    AlertUtils.showErrorNotification(MainActivity.this, message);
                }

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchableInfo searchableInfo = searchManager.getSearchableInfo(getComponentName());
        searchView.setSearchableInfo(searchableInfo);
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                Log.d(TAG, "onSuggestionSelect: " + position);
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                Log.d(TAG, "onSuggestionClick: " + position);

                EquipmentEntry equipmentEntry = suggestionEquipmentEntries.get(position);

                Intent intent = new Intent(MainActivity.this, EquipmentInfoActivity.class);
                String jsonString = new Gson().toJson(equipmentEntry);
                intent.putExtra(EquipmentInfoActivity.EQUIPMENT_EXTRA, jsonString);

                startActivity(intent);

                return true;
            }
        });
        suggestionsAdapter = new SuggestionsAdapter(this);
        searchView.setSuggestionsAdapter(suggestionsAdapter);
    }





    private void setupQrCodeButton() {
        View qrCodeButton = findViewById(R.id.qrCodeButton);
        qrCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "qrCodeButton onClick");

                if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, 1);
                }
                else {
                    Intent intent = new Intent(MainActivity.this, QRCodeReaderActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    private void setupEquipmentRegisterButton() {
        View equipmentRegisterButton = findViewById(R.id.equipmentRegister);
        equipmentRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "equipmentRegisterButton onClick");

                Intent intent = new Intent(MainActivity.this, ClientsActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.d(TAG, "onRequestPermissionsResult");
        if (grantResults != null &&
                grantResults.length > 0 &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Permission granted");

            Intent intent = new Intent(MainActivity.this, QRCodeReaderActivity.class);
            startActivity(intent);

        }
        else {
            Log.d(TAG, "Permission denied");
        }
    }

    private class SuggestionsAdapter extends CursorAdapter {

        private EquipmentsDataSource equipmentsDataSource;

        public SuggestionsAdapter(Context context) {
            super(context, null, 0);

            equipmentsDataSource = DatabaseFactory.getInstance(context).getEquipmentsDataSource();
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.search_suggestion_item, parent, false);;

            return row;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            int id = cursor.getInt(0);
            String query = cursor.getString(1);

            EquipmentEntry equipmentEntry = suggestionEquipmentEntries.get(id);

            TextView textView = (TextView)view.findViewById(R.id.itemTextView);

            if (equipmentEntry != null) {

                String text = equipmentEntry.getSerialNumber();

                if (query != null && query.length() > 0 && text.contains(query)) {

                    // This is to make part of the text bold
                    int index = text.indexOf(query);

                    String firstPart = text.substring(0, index);
                    String boldPart = text.substring(index, index + query.length());
                    String secondPart = text.substring(index + query.length());

                    Log.d(TAG, "first: " + firstPart + " bold: " + boldPart + " second: " + secondPart);

                    SpannableStringBuilder builder= new SpannableStringBuilder();
                    StyleSpan boldSpan = new StyleSpan(android.graphics.Typeface.BOLD);
                    builder.append(firstPart)
                            .append(boldPart, boldSpan, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                            .append(secondPart);

                    textView.setText(builder);
                }
                else {
                    textView.setText(text);
                }


            }
            else {
                textView.setText("ERROR");
            }

        }

        @Override
        public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
            Log.d(TAG, "runQueryOnBackgroundThread: " + constraint);

            MatrixCursor cursor = new MatrixCursor(new String[]{
                    BaseColumns._ID,
                    "search_string"
            });

            suggestionEquipmentEntries = equipmentsDataSource.getEquipmentsWithSearchQuery(constraint.toString());

            for (int i = 0; i < suggestionEquipmentEntries.size(); i++) {
                cursor.addRow(new Object[]{i, constraint});
            }

            return cursor;
        }
    }
}
