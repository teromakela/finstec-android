package fi.finstec.finstec.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toolbar;

import fi.finstec.finstec.BuildConfig;
import fi.finstec.finstec.R;
import fi.finstec.finstec.uihelpers.MenuActivity;

public class AboutActivity extends MenuActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        setupToolbar();

        // Set version number
        String vesionNumber = String.format(getString(R.string.version), BuildConfig.VERSION_NAME.toString());
        TextView versionNumberTextView = (TextView)findViewById(R.id.versionNumber);
        versionNumberTextView.setText(vesionNumber);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setActionBar(toolbar);
        setupToolbarMenu(toolbar);

        ImageButton backButton = (ImageButton)toolbar.findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SearchView searchView = (SearchView)toolbar.findViewById(R.id.searchView);
        searchView.setVisibility(View.GONE);
    }
}
