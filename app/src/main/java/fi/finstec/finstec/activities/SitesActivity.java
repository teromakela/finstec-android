package fi.finstec.finstec.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fi.finstec.finstec.R;
import fi.finstec.finstec.model.ClientEntry;
import fi.finstec.finstec.model.DatabaseFactory;
import fi.finstec.finstec.model.SiteEntry;
import fi.finstec.finstec.model.SitesDataSource;
import fi.finstec.finstec.uihelpers.MenuActivity;
import fi.finstec.finstec.uihelpers.SectionListBaseAdapter;
import fi.finstec.finstec.uihelpers.SectionListOnItemClickListener;
import fi.finstec.finstec.uihelpers.SectionListView;

public class SitesActivity extends MenuActivity {
    private static final String TAG = SitesActivity.class.getSimpleName();

    public static final String CLIENT_EXTRA = "clientExtra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sites);

        setupToolbar();

        SitesDataSource sitesDataSource = DatabaseFactory.getInstance(this).getSitesDataSource();

        // Get selected client
        String defValue= new Gson().toJson(new ClientEntry());
        String jsonString = getIntent().getExtras().getString(CLIENT_EXTRA, defValue);
        ClientEntry clientEntry = new Gson().fromJson(jsonString, ClientEntry.class);

        // Convert list to Object
        List<Object> items = new ArrayList<Object>(sitesDataSource.getSitesByClient(clientEntry));

        HashMap<String, List<Object>> listItems = new HashMap<String, List<Object>>();
        listItems.put(getString(R.string.sites), items);

        SiteListAdapter siteListAdapter = new SiteListAdapter(this, listItems, false);
        SectionListView sectionListView = (SectionListView)findViewById(R.id.listView);
        sectionListView.setAdapter(siteListAdapter);

        sectionListView.setSectionListOnItemClickListener(new SectionListOnItemClickListener() {
            @Override
            public void onItemClick(Object item) {

                SiteEntry siteEntry = (SiteEntry)item;

                Log.d(TAG, "onItemClick: " + siteEntry.getName());

                Intent intent = new Intent(SitesActivity.this, EquipmentsActivity.class);

                String jsonString = new Gson().toJson(siteEntry);
                intent.putExtra(EquipmentsActivity.SITE_EXTRA, jsonString);

                startActivity(intent);


            }
        });
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setActionBar(toolbar);
        setupToolbarMenu(toolbar);

        ImageButton backButton = (ImageButton)toolbar.findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SearchView searchView = (SearchView)toolbar.findViewById(R.id.searchView);
        searchView.setVisibility(View.GONE);
    }

    private class SiteListAdapter extends SectionListBaseAdapter {


        public SiteListAdapter(Context context, HashMap<String, List<Object>> listItems, boolean showSectionSeparator) {
            super(context, listItems, showSectionSeparator);
        }

        @Override
        protected View getItemView(LayoutInflater inflater, ViewGroup parent, Object item) {
            View row = inflater.inflate(R.layout.list_item_text, parent, false);

            SiteEntry siteEntry = (SiteEntry) item;

            // Configure View
            TextView itemTextView = (TextView)row.findViewById(R.id.itemTextView);
            itemTextView.setText(siteEntry.getName());

            return row;
        }

    }
}
