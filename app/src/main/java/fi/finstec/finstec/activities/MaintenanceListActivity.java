package fi.finstec.finstec.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import fi.finstec.finstec.R;
import fi.finstec.finstec.model.DatabaseFactory;
import fi.finstec.finstec.model.EquipmentEntry;
import fi.finstec.finstec.model.MaintenanceEntry;
import fi.finstec.finstec.model.MaintenancesDataSource;
import fi.finstec.finstec.uihelpers.MenuActivity;
import fi.finstec.finstec.uihelpers.SectionListBaseAdapter;
import fi.finstec.finstec.uihelpers.SectionListView;
import fi.finstec.finstec.utils.BitmapUtils;
import fi.finstec.finstec.utils.DateUtils;

public class MaintenanceListActivity extends MenuActivity {
    private static final String TAG = MaintenanceListActivity.class.getSimpleName();

    public static final String EQUIPMENT_EXTRA = "equipmentExtra";

    private EquipmentEntry equipmentEntry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintenance_list);

        String defValue= new Gson().toJson(new EquipmentEntry());
        String jsonString = getIntent().getExtras().getString(EQUIPMENT_EXTRA, defValue);
        equipmentEntry = new Gson().fromJson(jsonString, EquipmentEntry.class);

        Log.d(TAG, "onCreate equipmentId: " + equipmentEntry.getDatabaseId());

        setupToolbar();

        ////
        MaintenancesDataSource maintenancesDataSource = DatabaseFactory.getInstance(this).getMaintenancesDataSource();
        List<MaintenanceEntry> maintenanceEntries = maintenancesDataSource.getMaintenancesByEquipment(equipmentEntry);

        TextView noItemsTextView = (TextView)findViewById(R.id.emptyMaintenanceTextView);
        if (maintenanceEntries.size() == 0) {
            noItemsTextView.setVisibility(View.VISIBLE);
        }
        else {
            noItemsTextView.setVisibility(View.GONE);

            // Convert list to Object
            HashMap<String, List<Object>> listItems = new HashMap<String, List<Object>>();

            // Split by equipment type
            for (MaintenanceEntry maintenanceEntry: maintenanceEntries) {
                final Calendar calendar = new GregorianCalendar();
                calendar.setTime(maintenanceEntry.getDate());
                String year = Integer.toString(calendar.get(Calendar.YEAR));
                String localizedDate = String.format(getString(R.string.maintenance_history_section_title), year);


                List<Object> objects = listItems.get(localizedDate);

                if (objects != null) {
                    objects.add(maintenanceEntry);
                }
                else {
                    List<Object> newObjects = new ArrayList<Object>();
                    newObjects.add(maintenanceEntry);

                    listItems.put(localizedDate, newObjects);
                }
            }


            // Create the adapter
            MaintenanceListAdapter sectionListBaseAdapter = new MaintenanceListAdapter(this, listItems, true);

            SectionListView sectionListView = (SectionListView)findViewById(R.id.listView);
            sectionListView.setAdapter(sectionListBaseAdapter);
        }


    }

    private void setupToolbar() {
        android.widget.Toolbar toolbar = (android.widget.Toolbar)findViewById(R.id.toolbar);
        setActionBar(toolbar);
        setupToolbarMenu(toolbar);

        // Set title
        TextView title = (TextView)toolbar.findViewById(R.id.titleTextView);
        title.setText(String.format(getString(R.string.maintenance_history_title), equipmentEntry.getSerialNumber()));

        ImageButton backButton = (ImageButton)toolbar.findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SearchView searchView = (SearchView)toolbar.findViewById(R.id.searchView);
        searchView.setVisibility(View.GONE);
    }



    private class MaintenanceListAdapter extends SectionListBaseAdapter {


        public MaintenanceListAdapter(Context context, HashMap<String, List<Object>> listItems, boolean showSectionSeparator) {
            super(context, listItems, showSectionSeparator);
        }

        @Override
        protected View getItemView(LayoutInflater inflater, ViewGroup parent, Object item) {
            View row = inflater.inflate(R.layout.list_item_maintenance_history, parent, false);

            final MaintenanceEntry maintenanceEntry = (MaintenanceEntry)item;

            // Configure Date
            TextView dateTextView = (TextView)row.findViewById(R.id.dateTextView);
            dateTextView.setText(DateUtils.dateToMaintenanceHistoryDisplayString(maintenanceEntry.getDate()));

            // Configure operation type
            TextView operationTextView = (TextView)row.findViewById(R.id.operationTypeTextView);
            String localizedText = getString(getResources().getIdentifier(maintenanceEntry.getOperationType().toString(), "string", getPackageName()));
            operationTextView.setText(localizedText);

            // Configure person
            TextView personTextView = (TextView)row.findViewById(R.id.personTextView);
            String maintenanceBy = String.format(getString(R.string.maintenance_by), maintenanceEntry.getServicePerson());
            personTextView.setText(maintenanceBy);

            // Set comment
            TextView commentTextView = (TextView)row.findViewById(R.id.commentTextView);
            if (maintenanceEntry.getComment().length() != 0) {
                String comment = String.format("\"%1$s\"", maintenanceEntry.getComment());
                commentTextView.setText(comment);
                commentTextView.setVisibility(View.VISIBLE);
            }
            else {
                commentTextView.setVisibility(View.GONE);
            }

            // Set photo
            ImageView photoViewImage = (ImageView) row.findViewById(R.id.photoImageView);

            byte[] imgByte = maintenanceEntry.getPhotoThumbnail();
            if (imgByte != null) {
                Bitmap thumbnailBitmap = BitmapFactory.decodeByteArray(imgByte, 0, imgByte.length);
                if (thumbnailBitmap != null) {
                    photoViewImage.setImageBitmap(thumbnailBitmap);
                    photoViewImage.setVisibility(View.VISIBLE);
                    photoViewImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(MaintenanceListActivity.this, MaintenancePhotoActivity.class);
                            intent.putExtra(MaintenancePhotoActivity.PHOTO_PATH_EXTRA, maintenanceEntry.getPhotoPath());

                            startActivity(intent);
                        }
                    });
                }
                else {
                    photoViewImage.setVisibility(View.GONE);
                }
            }
            else {
                photoViewImage.setVisibility(View.GONE);
            }


            return row;
        }

    }

}
