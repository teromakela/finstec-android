package fi.finstec.finstec.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.Toolbar;
import android.os.Vibrator;
import android.content.Context;

import com.google.gson.Gson;

import fi.finstec.finstec.R;
import fi.finstec.finstec.model.DatabaseFactory;
import fi.finstec.finstec.model.EquipmentEntry;
import fi.finstec.finstec.model.EquipmentsDataSource;
import fi.finstec.finstec.utils.AlertUtils;
import fi.finstec.finstec.utils.ConfirmationDialogCallback;
import github.nisrulz.qreader.QRDataListener;
import github.nisrulz.qreader.QREader;

public class QRCodeReaderActivity extends Activity {
    private final static String TAG = QRCodeReaderActivity.class.getSimpleName();

    private SurfaceView surfaceView;
    private QREader qrEader;

    private boolean processingQRRead;

    Vibrator vibrator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode_reader);

        setupToolbar();

        vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);

        processingQRRead = false;

        surfaceView = (SurfaceView) findViewById(R.id.camera_view);

        final EquipmentsDataSource equipmentsDataSource = DatabaseFactory.getInstance(this).getEquipmentsDataSource();

        qrEader = new QREader.Builder(this, surfaceView, new QRDataListener() {
            @Override public void onDetected(final String data) {
                // Do something with the string data

                // Go to the next activity
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "Value : " + data);

                        if (processingQRRead) {
                            return;
                        }
                        processingQRRead = true;

                        EquipmentEntry equipmentEntry = equipmentsDataSource.getEquipmentBySerialNumber(data);

                        if (equipmentEntry != null) {
                            // Don't keep reading
                            qrEader.stop();

                            vibrator.vibrate(300);

                            Intent intent = new Intent(QRCodeReaderActivity.this, EquipmentInfoActivity.class);
                            String jsonString = new Gson().toJson(equipmentEntry);
                            intent.putExtra(EquipmentInfoActivity.EQUIPMENT_EXTRA, jsonString);

                            startActivity(intent);
                        }
                        else {
                            Log.d(TAG, "No equipment found with localid: " + data);

                            String message = String.format(getString(R.string.cant_find_equipment_with_id), data);

                            AlertUtils.showErrorNotificationWithCallback(QRCodeReaderActivity.this, message, new ConfirmationDialogCallback() {
                                @Override
                                public void dialogFinished(boolean confirmation) {
                                    // resume reading
                                    processingQRRead = false;
                                }
                            });
                        }



                    }
                });

            }
        }).build();

        qrEader.init();

    }

    @Override
    protected void onResume() {
        super.onResume();

        // resume reading
        processingQRRead = false;
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();

        if (qrEader != null) {
            qrEader.start();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (qrEader != null) {
            qrEader.stop();
            qrEader.releaseAndCleanup();
        }
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setActionBar(toolbar);

        ImageButton backButton = (ImageButton)toolbar.findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SearchView searchView = (SearchView)toolbar.findViewById(R.id.searchView);
        searchView.setVisibility(View.GONE);
    }
}
