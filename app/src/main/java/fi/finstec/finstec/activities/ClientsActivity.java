package fi.finstec.finstec.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fi.finstec.finstec.R;
import fi.finstec.finstec.model.ClientEntry;
import fi.finstec.finstec.model.ClientsDataSource;
import fi.finstec.finstec.model.DatabaseFactory;
import fi.finstec.finstec.uihelpers.MenuActivity;
import fi.finstec.finstec.uihelpers.SectionListBaseAdapter;
import fi.finstec.finstec.uihelpers.SectionListOnItemClickListener;
import fi.finstec.finstec.uihelpers.SectionListView;

public class ClientsActivity extends MenuActivity {
    private static final String TAG = ClientsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clients);

        setupToolbar();

        ClientsDataSource clientsDataSource = DatabaseFactory.getInstance(this).getClientsDataSource();

        // Convert list to Object
        List<Object> items = new ArrayList<Object>(clientsDataSource.getAllClients());

        HashMap<String, List<Object>> listItems = new HashMap<String, List<Object>>();
        listItems.put(getString(R.string.clients), items);


        // Create the adapter
        ClientListAdapter sectionListBaseAdapter = new ClientListAdapter(this, listItems, false);

        SectionListView sectionListView = (SectionListView)findViewById(R.id.listView);
        sectionListView.setAdapter(sectionListBaseAdapter);

        sectionListView.setSectionListOnItemClickListener(new SectionListOnItemClickListener() {
            @Override
            public void onItemClick(Object item) {

                ClientEntry clientEntry = (ClientEntry)item;

                Log.d(TAG, "onItemClick: " + clientEntry.getName());

                Intent intent = new Intent(ClientsActivity.this, SitesActivity.class);

                String jsonString = new Gson().toJson(clientEntry);
                intent.putExtra(SitesActivity.CLIENT_EXTRA, jsonString);

                startActivity(intent);
            }
        });

    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setActionBar(toolbar);
        setupToolbarMenu(toolbar);

        ImageButton backButton = (ImageButton)toolbar.findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SearchView searchView = (SearchView)toolbar.findViewById(R.id.searchView);
        searchView.setVisibility(View.GONE);
    }


    private class ClientListAdapter extends SectionListBaseAdapter {


        public ClientListAdapter(Context context, HashMap<String, List<Object>> listItems, boolean showSectionSeparator) {
            super(context, listItems, showSectionSeparator);
        }

        @Override
        protected View getItemView(LayoutInflater inflater, ViewGroup parent, Object item) {
            View row = inflater.inflate(R.layout.list_item_text, parent, false);

            ClientEntry clientEntry = (ClientEntry)item;

            // Configure View
            TextView itemTextView = (TextView)row.findViewById(R.id.itemTextView);
            itemTextView.setText(clientEntry.getName());

            return row;
        }

    }
}
