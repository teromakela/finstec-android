package fi.finstec.finstec.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fi.finstec.finstec.R;
import fi.finstec.finstec.model.ClientEntry;
import fi.finstec.finstec.model.DatabaseFactory;
import fi.finstec.finstec.model.EquipmentEntry;
import fi.finstec.finstec.model.EquipmentsDataSource;
import fi.finstec.finstec.model.SiteEntry;
import fi.finstec.finstec.uihelpers.MenuActivity;
import fi.finstec.finstec.uihelpers.SectionListBaseAdapter;
import fi.finstec.finstec.uihelpers.SectionListOnItemClickListener;
import fi.finstec.finstec.uihelpers.SectionListView;

public class EquipmentsActivity extends MenuActivity {
    private static final String TAG = EquipmentsActivity.class.getSimpleName();

    public static final String SITE_EXTRA = "siteExtra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipments);

        setupToolbar();

        String defValue= new Gson().toJson(new SiteEntry());
        String jsonString = getIntent().getExtras().getString(SITE_EXTRA, defValue);
        SiteEntry siteEntry = new Gson().fromJson(jsonString, SiteEntry.class);

        EquipmentsDataSource equipmentsDataSource = DatabaseFactory.getInstance(this).getEquipmentsDataSource();

        HashMap<String, List<Object>> listItems = new HashMap<String, List<Object>>();

        List<EquipmentEntry> equipmentEntries = equipmentsDataSource.getEquipmentsBySite(siteEntry);

        // Split by equipment type
        for (EquipmentEntry equipmentEntry: equipmentEntries) {
            String equipmentType = equipmentEntry.getEquipmentType().toString();
            String localizedType = getString(getResources().getIdentifier(equipmentType, "string", getPackageName()));

            List<Object> objects = listItems.get(localizedType);

            if (objects != null) {
                objects.add(equipmentEntry);
            }
            else {
                List<Object> newObjects = new ArrayList<Object>();
                newObjects.add(equipmentEntry);

                listItems.put(localizedType, newObjects);
            }
        }

        EquipmentListAdapter equipmentListAdapter = new EquipmentListAdapter(this, listItems, true);
        SectionListView sectionListView = (SectionListView)findViewById(R.id.listView);
        sectionListView.setAdapter(equipmentListAdapter);

        sectionListView.setSectionListOnItemClickListener(new SectionListOnItemClickListener() {
            @Override
            public void onItemClick(Object item) {

                EquipmentEntry equipmentEntry = (EquipmentEntry)item;

                Log.d(TAG, "onItemClick: " + equipmentEntry.getModel());

                Intent intent = new Intent(EquipmentsActivity.this, EquipmentInfoActivity.class);
                String jsonString = new Gson().toJson(equipmentEntry);
                intent.putExtra(EquipmentInfoActivity.EQUIPMENT_EXTRA, jsonString);

                startActivity(intent);


            }
        });
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setActionBar(toolbar);
        setupToolbarMenu(toolbar);

        ImageButton backButton = (ImageButton)toolbar.findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SearchView searchView = (SearchView)toolbar.findViewById(R.id.searchView);
        searchView.setVisibility(View.GONE);
    }

    private class EquipmentListAdapter extends SectionListBaseAdapter {


        public EquipmentListAdapter(Context context, HashMap<String, List<Object>> listItems, boolean showSectionSeparator) {
            super(context, listItems, showSectionSeparator);
        }

        @Override
        protected View getItemView(LayoutInflater inflater, ViewGroup parent, Object item) {
            View row = inflater.inflate(R.layout.list_item_equipment, parent, false);

            EquipmentEntry equipmentEntry = (EquipmentEntry) item;

            // Configure View
            TextView titleTextView = (TextView)row.findViewById(R.id.titleTextView);
            titleTextView.setText(equipmentEntry.getSerialNumber());

            TextView subtitleTextView = (TextView)row.findViewById(R.id.subtitleTextView);
            String subtitle = equipmentEntry.getMake() + " " + equipmentEntry.getModel();
            subtitleTextView.setText(subtitle);

            return row;
        }

    }
}
