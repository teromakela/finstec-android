package fi.finstec.finstec.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.CalendarContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fi.finstec.finstec.R;
import fi.finstec.finstec.core.AppSettings;
import fi.finstec.finstec.model.DatabaseFactory;
import fi.finstec.finstec.model.EquipmentEntry;
import fi.finstec.finstec.model.MaintenanceEntry;
import fi.finstec.finstec.model.MaintenancesDataSource;
import fi.finstec.finstec.uihelpers.ExpansionPanel;
import fi.finstec.finstec.uihelpers.MenuActivity;
import fi.finstec.finstec.uihelpers.OnExpansionPanelSelectedListener;
import fi.finstec.finstec.utils.BitmapUtils;
import fi.finstec.finstec.utils.DateUtils;

public class NewMaintenanceActivity extends MenuActivity {
    private final static String TAG = NewMaintenanceActivity.class.getSimpleName();

    public static final String EQUIPMENT_EXTRA = "equipmentExtra";
    private static final int REQUEST_IMAGE_CAPTURE = 1;


    private EquipmentEntry equipmentEntry;

    private ExpansionPanel operationExpansionPanel;
    private String operationType;
    private Date date;
    private Date nextMaintenance;
    private String photo;
    private byte[] photoThumbnail;

    private String mCurrentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_maintenance);

        String defValue= new Gson().toJson(new EquipmentEntry());
        String jsonString = getIntent().getExtras().getString(EQUIPMENT_EXTRA, defValue);
        equipmentEntry = new Gson().fromJson(jsonString, EquipmentEntry.class);

        setupToolbar();

        setupDate();
        setupOperation();
        setupPerson();
        setupNextMaintenance();
        setupPhoto();
        setupAddMaintenanceButton();

        // Don't show keyboard when activity opens
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setActionBar(toolbar);
        setupToolbarMenu(toolbar);

        // Set title
        TextView title = (TextView)toolbar.findViewById(R.id.titleTextView);
        title.setText(String.format(getString(R.string.maintenance_title), equipmentEntry.getSerialNumber()));

        ImageButton backButton = (ImageButton)toolbar.findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SearchView searchView = (SearchView)toolbar.findViewById(R.id.searchView);
        searchView.setVisibility(View.GONE);
    }

    private void setupDate() {
        date = new Date();

        final TextView dateTextView = (TextView)findViewById(R.id.dateTextView);
        dateTextView.setText(DateUtils.dateToDisplayString(date));

        View dateView = findViewById(R.id.dateView);
        dateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar calendar = new GregorianCalendar();
                calendar.setTime(date);

                DatePickerDialog datePickerDialog = new DatePickerDialog(NewMaintenanceActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        Log.d(TAG, "selected date: " + dayOfMonth + "." + monthOfYear + "." + year);

                        calendar.set(year, monthOfYear, dayOfMonth);
                        date = calendar.getTime();

                        // Update textview
                        dateTextView.setText(DateUtils.dateToDisplayString(date));

                        // Update the next maintenance with 6 months more than date
                        updateNextMaintenance();


                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.show();
            }
        });
    }

    private void setupNextMaintenance() {

        // Initially, set date
        updateNextMaintenance();

        final TextView nextMaintenanceTextView = (TextView)findViewById(R.id.nextMaintenanceTextView);
        nextMaintenanceTextView.setText(DateUtils.dateToDisplayString(nextMaintenance));

        View nextMaintenanceView = findViewById(R.id.nextMaintenanceView);
        nextMaintenanceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar calendar = new GregorianCalendar();
                calendar.setTime(nextMaintenance);

                DatePickerDialog datePickerDialog = new DatePickerDialog(NewMaintenanceActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        Log.d(TAG, "selected date: " + dayOfMonth + "." + monthOfYear + "." + year);

                        calendar.set(year, monthOfYear, dayOfMonth);
                        nextMaintenance = calendar.getTime();

                        // Update textview
                        nextMaintenanceTextView.setText(DateUtils.dateToDisplayString(nextMaintenance));

                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.show();
            }
        });
    }

    private void setupOperation() {

        List<String> array = new ArrayList<String>();
        for (MaintenanceEntry.OperationTypes operationType: MaintenanceEntry.OperationTypes.values()) {
            array.add(operationType.toString());
        }

        operationExpansionPanel = (ExpansionPanel) findViewById(R.id.operationExpansionPanel);
        operationExpansionPanel.setOnExpansionPanelSelectedListener(new OnExpansionPanelSelectedListener() {
            @Override
            public void onItemSelected(String selected) {
                Log.d(TAG, "operationExpansionPanel onItemSelected");
                operationType = selected;

                updateNextMaintenance();
            }
        });
        operationExpansionPanel.setupOptionList(array, 0);
    }

    private void setupPhoto() {
        photo = null;
        View photoView = findViewById(R.id.photoView);

        View photoThumbnailView = findViewById(R.id.photoThumbnailImageView);
        photoThumbnailView.setVisibility(View.GONE);

        photoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(NewMaintenanceActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(NewMaintenanceActivity.this, new String[]{Manifest.permission.CAMERA}, 1);
                }
                else {
                    takePicture();
                }

            }
        });
    }

    private void setupPerson() {
        AppSettings appSettings = AppSettings.getInstance(this);

        TextView personTextView = (TextView)findViewById(R.id.servicePersonTextView);
        personTextView.setText(appSettings.getUserName());
    }

    private void updateNextMaintenance() {
        Log.d(TAG, "updateNextMaintenance: " + operationType);

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);

        Integer nextMaintenanceMonths = 0;
        Iterator it = equipmentEntry.getMaintenanceInterval().entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            Log.d(TAG, "key : " + pair.getKey() + " value: " + pair.getValue());
            if (pair.getKey().equals(operationType)) {
                nextMaintenanceMonths = (Integer) pair.getValue();
            }
        }

        Log.d(TAG, "nextMaintenanceMonths: " + nextMaintenanceMonths);


        calendar.add(Calendar.MONTH, nextMaintenanceMonths);
        nextMaintenance = calendar.getTime();
        TextView nextMaintenanceTextView = (TextView)findViewById(R.id.nextMaintenanceTextView);
        nextMaintenanceTextView.setText(DateUtils.dateToDisplayString(nextMaintenance));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            Log.d(TAG, "mCurrentPhotoPath: " + mCurrentPhotoPath);

            Bitmap thumbnailBitmap = BitmapUtils.getRotatedBitmap(NewMaintenanceActivity.this, mCurrentPhotoPath);
            if (thumbnailBitmap == null) {
                Log.e(TAG, "Error reading image: " + mCurrentPhotoPath);
                return;
            }

            ImageView thumbImageView = (ImageView)findViewById(R.id.photoThumbnailImageView);
            thumbImageView.setVisibility(View.VISIBLE);
            thumbnailBitmap = BitmapUtils.scaleBitmapAndKeepRation(thumbnailBitmap, 500, 500);
            thumbImageView.setImageBitmap(thumbnailBitmap);

            final float scale = Resources.getSystem().getDisplayMetrics().density;
            float px = 120 * scale + 0.5f;
            Log.d(TAG, "needed thumbnail size in px: " + px);

            ImageView logoImageView = (ImageView)findViewById(R.id.photoLogoImageView);
            logoImageView.setVisibility(View.GONE);

            photo = mCurrentPhotoPath;

            // Save the thumbnail
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            thumbnailBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            photoThumbnail = stream.toByteArray();

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.d(TAG, "onRequestPermissionsResult");
        if (grantResults != null &&
                grantResults.length > 0 &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Permission granted");

            takePicture();

        }
        else {
            Log.d(TAG, "Permission denied");
        }
    }

    private void setupAddMaintenanceButton() {
        View addMaintenanceButton = findViewById(R.id.addMaintenanceButton);
        addMaintenanceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "addMaintenanceButton clicked");

                // Type
                MaintenanceEntry.OperationTypes type = MaintenanceEntry.OperationTypes.fromString(operationExpansionPanel.getSelectedText());

                // Comment
                String comment = ((TextView)findViewById(R.id.commentEditText)).getText().toString();
                String servicePerson = ((TextView)findViewById(R.id.servicePersonTextView)).getText().toString();

                Log.d(TAG, "items:" +
                        " .Operation type: " + type +
                        " .Date: " + date +
                        " .Comment: " + comment +
                        " .Service person: " + servicePerson +
                        " .Next maintenance: " + nextMaintenance +
                        " .hasPhoto: " + (photo != null));

                MaintenanceEntry maintenanceEntry = new MaintenanceEntry();
                maintenanceEntry.setEquipment(equipmentEntry);
                maintenanceEntry.setOperationType(type);
                maintenanceEntry.setDate(date);
                maintenanceEntry.setComment(comment);
                maintenanceEntry.setServicePerson(servicePerson);
                maintenanceEntry.setNextMaintenance(nextMaintenance);
                maintenanceEntry.setPhotoPath(photo);
                maintenanceEntry.setPhotoThumbnail(photoThumbnail);

                MaintenancesDataSource maintenancesDataSource = DatabaseFactory.getInstance(NewMaintenanceActivity.this).getMaintenancesDataSource();
                maintenanceEntry = maintenancesDataSource.addMaintenance(maintenanceEntry);
                if (maintenanceEntry == null) {
                    Log.d(TAG, "Error saving maintenance");
                }
                else {
                    Log.d(TAG, "Saving maintenance successfully: " + maintenanceEntry.getEquipment());
                }


                // Show snackbar to indicate success
                View coordinatorLayout = findViewById(R.id.snackbarPosition);
                Snackbar
                        .make(coordinatorLayout, R.string.maintenance_saved_successfully, Snackbar.LENGTH_LONG)
                        .setCallback(new Snackbar.Callback() {
                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                super.onDismissed(snackbar, event);
                                finish();
                            }
                        })
                        .show();

            }
        });
    }


    private void takePicture() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.d(TAG, "Error creating photo file");
            }

            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = Uri.fromFile(photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }

        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
//        Log.d(TAG, "createImageFile mCurrentPhotoPath: " + mCurrentPhotoPath);
        return image;
    }

}
