package fi.finstec.finstec.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;

import fi.finstec.finstec.R;

/**
 * Created by walter on 9/7/16.
 */
public class AlertUtils {
    private final static String TAG = AlertUtils.class.getSimpleName();

    public static void showErrorNotification(final Activity activity, final String message) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!activity.isFinishing()){
                    new AlertDialog.Builder(activity)
                            .setTitle(activity.getString(R.string.error_popup_title))
                            .setMessage(message)
                            .setCancelable(false)
                            .setPositiveButton(activity.getString(R.string.error_popup_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // Whatever...
                                }
                            }).create().show();
                }
                else {
                    Log.e(TAG, "Error showing notification because activity is finishing");
                }
            }
        });
    }

    public static void showErrorNotificationWithCallback(final Activity activity,
                                                         final String message,
                                                         final ConfirmationDialogCallback callback) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!activity.isFinishing()){

                    new AlertDialog.Builder(activity)
                            .setTitle(activity.getString(R.string.error_popup_title))
                            .setMessage(message)
                            .setCancelable(false)
                            .setPositiveButton(activity.getString(R.string.error_popup_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    if (callback != null) {
                                        callback.dialogFinished(true);
                                    }
                                }
                            }).create().show();
                }
                else {
                    Log.e(TAG, "Error showing notification because activity is finishing");
                }
            }
        });
    }
}
