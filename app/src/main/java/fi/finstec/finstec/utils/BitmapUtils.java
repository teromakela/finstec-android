package fi.finstec.finstec.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.io.IOException;

import fi.finstec.finstec.R;

/**
 * Created by walter on 9/13/16.
 */
public class BitmapUtils {
    private final static String TAG = BitmapUtils.class.getSimpleName();


    public static Bitmap scaleBitmapAndKeepRation(Bitmap targetBmp, int reqHeightInPixels, int reqWidthInPixels)  {

        if (reqWidthInPixels <= 0 || reqHeightInPixels <= 0) {
            Log.e(TAG, "Error resizing image to dimentions: " + reqWidthInPixels + " x " + reqHeightInPixels);
            return null;
        }

        Matrix m = new Matrix();
        m.setRectToRect(new RectF(0, 0, targetBmp.getWidth(), targetBmp.getHeight()), new RectF(0, 0, reqWidthInPixels, reqHeightInPixels), Matrix.ScaleToFit.CENTER);
        Bitmap scaledBitmap = Bitmap.createBitmap(targetBmp, 0, 0, targetBmp.getWidth(), targetBmp.getHeight(), m, true);
        return scaledBitmap;
    }

    public static Bitmap getRotatedBitmap(Activity activity, String imagePath) {
        try {
            String path = Uri.parse(imagePath).getPath();
            Log.d(TAG, "path: " + path);
            ExifInterface exif = new ExifInterface(path);
            int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            Log.d(TAG, "exifOrientation: " + exifOrientation);

            Bitmap bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), Uri.parse(imagePath));

            Matrix matrix = new Matrix();
            switch (exifOrientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.postRotate(90);
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0,
                            bitmap.getWidth(), bitmap.getHeight(),
                            matrix, true);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.postRotate(180);
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0,
                            bitmap.getWidth(), bitmap.getHeight(),
                            matrix, true);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.postRotate(270);
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0,
                            bitmap.getWidth(), bitmap.getHeight(),
                            matrix, true);
                    break;
            }

            return bitmap;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public static int getToolbarImage(String make, String model) {
        if (make.equalsIgnoreCase("celitron") && model.equalsIgnoreCase("ISS-575")) {
            return R.drawable._celitron_iss_575;
        }
        else if (make.equalsIgnoreCase("Finn-Aqua") && model.equalsIgnoreCase("A-669 D")) {
            return R.drawable._finn_aqua_a_669;
        }
        else if (make.equalsIgnoreCase("AT-OS") && model.equalsIgnoreCase("AWD-655-10")) {
            return R.drawable._at_os_awd_655;
        }
        else if (make.equalsIgnoreCase("Celitron") && model.equalsIgnoreCase("A-456")) {
            return R.drawable._celitron_a_456;
        }
        else if (make.equalsIgnoreCase("Purecleer") && model.equalsIgnoreCase("RO-3500")) {
            return R.drawable._purecleer_ro_3500;
        }
        else if (make.equalsIgnoreCase("Celitron") && model.equalsIgnoreCase("AC-450")) {
            return R.drawable._celitron_ac_450;
        }
        else if (make.equalsIgnoreCase("AT-OS") && model.equalsIgnoreCase("AF65-EW")) {
            return R.drawable._at_os_awd_65_ew;
        }
        else {
            return R.drawable.autoclav;
        }
    }
}
