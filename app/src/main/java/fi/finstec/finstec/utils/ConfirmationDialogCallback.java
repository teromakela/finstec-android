package fi.finstec.finstec.utils;

/**
 * Created by walter on 9/7/16.
 */
public interface ConfirmationDialogCallback {
    void dialogFinished(boolean confirmation);
}
