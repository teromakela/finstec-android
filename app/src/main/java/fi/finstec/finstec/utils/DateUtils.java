package fi.finstec.finstec.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by walter on 9/7/16.
 */
public class DateUtils {

    private static final String DATABASE_DATE_FORMAT = "yyyy-MM-dd hh:mm:ss";
    private static final String DISPLAY_DATE_FORMAT = "dd.MM.yyyy";
    private static final String MAINTENANCE_HISTORY_DISPLAY_DATE_FORMAT = "dd.MM.";


    public static String dateToDatabaseString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATABASE_DATE_FORMAT, Locale.US);
        return dateFormat.format(date);
    }

    public static Date databaseStringToDate(String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATABASE_DATE_FORMAT, Locale.US);
        try {
            Date date = dateFormat.parse(dateString);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String dateToDisplayString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DISPLAY_DATE_FORMAT, Locale.US);
        return dateFormat.format(date);
    }

    public static String dateToMaintenanceHistoryDisplayString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(MAINTENANCE_HISTORY_DISPLAY_DATE_FORMAT, Locale.US);
        return dateFormat.format(date);
    }
}
