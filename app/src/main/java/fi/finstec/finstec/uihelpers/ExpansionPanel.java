package fi.finstec.finstec.uihelpers;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import fi.finstec.finstec.R;

/**
 * Created by walter on 9/10/16.
 */
public class ExpansionPanel extends RelativeLayout {
    private static final String TAG = ExpansionPanel.class.getSimpleName();

    private Context context;
    private boolean expanded;
    private LayoutInflater inflater;
    private LinearLayout optionList;
    private TextView selectedOption;
    private ImageView moreLessButton;
    private String selectedText;

    private OnExpansionPanelSelectedListener onExpansionPanelSelectedListener;

    public ExpansionPanel(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;

        inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        ViewGroup mainView = (ViewGroup)inflater.inflate( R.layout.expansion_panel_with_list, null );

        selectedOption = (TextView) mainView.findViewById(R.id.selectedOption);
        moreLessButton = (ImageView)mainView.findViewById(R.id.moreLessButton);
        optionList = (LinearLayout)mainView.findViewById(R.id.optionList);

        setupMoreLessButton(mainView);

        // Initial state
        expanded = false;
        optionList.setVisibility(View.GONE);

        addView(mainView);

    }

    public void setOnExpansionPanelSelectedListener(OnExpansionPanelSelectedListener onExpansionPanelSelectedListener) {
        this.onExpansionPanelSelectedListener = onExpansionPanelSelectedListener;
    }


    private void setupMoreLessButton(View mainView) {
        mainView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "mainView onClick: " + expanded);

                if (expanded) {
                    closeExpansionPanel();
                }
                else {
                    openExpansionPanel();
                }
            }
        });
    }

    public String getSelectedText() {
        return selectedText;
    }

    public void setupOptionList(final List<String> options, int defaultPosition) {

        for (int i = 0; i < options.size(); i++) {
            View item = inflater.inflate(R.layout.expansion_panel_item, null);
            item.setTag(i);

            TextView textView = (TextView)item.findViewById(R.id.optionTextView);
            String localizedText = context.getString(getResources().getIdentifier(options.get(i), "string", context.getPackageName()));
            textView.setText(localizedText);

            item.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "item clicked: " + v.getTag());

                    // Set selected option
                    selectedText = options.get( (Integer)v.getTag() );
                    String localizedText = context.getString(getResources().getIdentifier(selectedText, "string", context.getPackageName()));

                    selectedOption.setText(localizedText);

                    closeExpansionPanel();
                }
            });


            optionList.addView(item);
        }

        // Set selected option
        selectedText = options.get(defaultPosition);
        String localizedText = context.getString(getResources().getIdentifier(selectedText, "string", context.getPackageName()));

        selectedOption.setText(localizedText);

        closeExpansionPanel();
    }

    private void openExpansionPanel() {
        Log.d(TAG, "openExpansionPanel");

        expanded = true;

        selectedOption.setVisibility(View.GONE);
        optionList.setVisibility(View.VISIBLE);

        moreLessButton.setBackgroundResource(R.drawable.ic_expand_less_black_24dp);
    }

    private void closeExpansionPanel() {
        Log.d(TAG, "closeExpansionPanel");

        expanded = false;

        selectedOption.setVisibility(View.VISIBLE);
        optionList.setVisibility(View.GONE);

        moreLessButton.setBackgroundResource(R.drawable.ic_expand_more_black_24dp);

        if (onExpansionPanelSelectedListener != null) {
            onExpansionPanelSelectedListener.onItemSelected(selectedText);
        }
    }
}
