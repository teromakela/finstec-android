package fi.finstec.finstec.uihelpers;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fi.finstec.finstec.R;

/**
 * Created by walter on 9/7/16.
 */
public abstract class SectionListBaseAdapter extends BaseAdapter {
    private static final String TAG = SectionListBaseAdapter.class.getSimpleName();

    private enum ListTypes {
        TYPE_HEADER,
        TYPE_ITEM,
        TYPE_SEPARATOR
    }

    Context context;
    HashMap<String, List<Object>> listItems;
    List<ListItem> flatItems;

    public SectionListBaseAdapter(Context context, HashMap<String, List<Object>> listItems, boolean showSectionSeparator) {
        this.context = context;
        this.listItems = listItems;

        // Change from the hashmap to a flat list
        flatItems = new ArrayList<ListItem>();

        Iterator it = listItems.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry mapEntry = (Map.Entry)it.next();

            // Add the header
            flatItems.add(new ListItem(ListTypes.TYPE_HEADER, mapEntry.getKey()));

            // Add each item
            for (Object item: (List<Object>)mapEntry.getValue()) {
                flatItems.add(new ListItem(ListTypes.TYPE_ITEM, item));
            }

            if (showSectionSeparator) {
                // Add separator
                flatItems.add(new ListItem(ListTypes.TYPE_SEPARATOR, null));
            }

        }

        // Test
        for (ListItem item: flatItems) {
            Log.d(TAG, "item type: " + item.type.toString());
        }
    }


    @Override
    public int getCount() {
        return flatItems.size();
    }

    @Override
    public ListItem getItem(int position) {
        return flatItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean isEnabled(int position) {
        ListItem item = getItem(position);

        // Only items are clickable, not headers or separators
        if (item.type == ListTypes.TYPE_ITEM) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = null;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final ListItem listItem = getItem(position);

        switch (listItem.type) {
            case TYPE_HEADER:
            {
                row = inflater.inflate(R.layout.list_section_header, parent, false);

                // Configure row
                TextView title = (TextView)row.findViewById(R.id.sectionTextView);
                title.setText((String)listItem.item);

                break;
            }
            case TYPE_ITEM:
            {
//                row = inflater.inflate(R.layout.list_item_text, parent, false);
                row = getItemView(inflater, parent, listItem.item);

                break;
            }
            case TYPE_SEPARATOR:
            {
                row = inflater.inflate(R.layout.list_separator, parent, false);

                break;
            }

        }

        return row;
    }

    public Object getObject(int position) {
        ListItem item = getItem(position);
        return item.item;
    }

    protected abstract View getItemView(LayoutInflater inflater, ViewGroup parent, Object item);


    private class ListItem {

        public ListTypes type;
        public Object item;

        public ListItem(ListTypes type, Object item) {
            this.type = type;
            this.item = item;
        }
    }
}
