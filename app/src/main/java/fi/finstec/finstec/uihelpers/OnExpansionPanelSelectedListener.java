package fi.finstec.finstec.uihelpers;

/**
 * Created by walter on 9/14/16.
 */
public interface OnExpansionPanelSelectedListener {
    void onItemSelected(String selected);
}
