package fi.finstec.finstec.uihelpers;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * Created by walter on 9/7/16.
 */
public class SectionListView extends ListView {
    private static final String TAG = SectionListView.class.getSimpleName();

    private SectionListOnItemClickListener sectionListOnItemClickListener;

    public SectionListView(Context context, AttributeSet attrs) {
        super(context, attrs);

        sectionListOnItemClickListener = null;

        // No divider
        setDivider(null);


        setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "item clicked: " + position);

                if (sectionListOnItemClickListener != null) {
                    Object item = ((SectionListBaseAdapter)getAdapter()).getObject(position);

                    sectionListOnItemClickListener.onItemClick(item);
                }
            }
        });
    }

    public void setSectionListOnItemClickListener(SectionListOnItemClickListener sectionListOnItemClickListener) {
        this.sectionListOnItemClickListener = sectionListOnItemClickListener;
    }

    public void setAdapter(SectionListBaseAdapter adapter) {
        super.setAdapter(adapter);
    }

}
