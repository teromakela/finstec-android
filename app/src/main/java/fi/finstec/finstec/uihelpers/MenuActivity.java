package fi.finstec.finstec.uihelpers;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toolbar;

import fi.finstec.finstec.R;
import fi.finstec.finstec.activities.AboutActivity;
import fi.finstec.finstec.activities.LoginActivity;
import fi.finstec.finstec.core.AppSettings;

/**
 * Created by walter on 9/11/16.
 */
public class MenuActivity extends AppCompatActivity {
    private final static String TAG = MenuActivity.class.getSimpleName();

    protected void setupToolbarMenu(Toolbar toolbar) {
        toolbar.showOverflowMenu();
    }

    protected void setupToolbarMenu(android.support.v7.widget.Toolbar toolbar) {
        toolbar.showOverflowMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_sync) {
            Log.d(TAG, "menu item clicked: sync");
            return true;
        }
        else if (id == R.id.action_logout) {
            Log.d(TAG, "menu item clicked: logout");

            AppSettings appSettings = AppSettings.getInstance(this);
            appSettings.setUserToken("");

            Intent intent = new Intent(this, LoginActivity.class);

            finishAffinity();
            startActivity(intent);

            return true;
        }
        else if (id == R.id.action_about) {
            Log.d(TAG, "menu item clicked: about");

            // Avoid loading about multiple times
            if (!(this instanceof AboutActivity)) {
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
            }


        }

        return super.onOptionsItemSelected(item);
    }
}
