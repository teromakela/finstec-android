package fi.finstec.finstec.uihelpers;

/**
 * Created by walter on 9/7/16.
 */
public interface SectionListOnItemClickListener {

    void onItemClick(Object item);
}
