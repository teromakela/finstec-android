package fi.finstec.finstec.core;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * Created by walter on 9/11/16.
 */
public class AppSettings {

    private static final String SHARED_PREFS_NAME = "FinstecPrefs";

    private static final String USER_NAME = "userName";
    private static final String USER_TOKEN = "taxiAvailabilityAlreadyShown";


    SharedPreferences prefs;

    private static AppSettings instance = null;
    public static AppSettings getInstance(Context context) {
        if (instance == null) {
            instance = new AppSettings(context);
        }

        return instance;
    }

    private AppSettings(Context context) {
        prefs = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
    }

    public String getUserName() {
        return prefs.getString(USER_NAME, "");
    }

    public void setUserName(String newValue) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(USER_NAME, newValue);
        editor.commit();
    }

    public String getUserToken() {
        return prefs.getString(USER_TOKEN, "");
    }

    public void setUserToken(String newValue) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(USER_TOKEN, newValue);
        editor.commit();
    }


}

