package fi.finstec.finstec;

import android.content.Context;

import org.apache.maven.model.Site;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.util.List;

import fi.finstec.finstec.model.ClientEntry;
import fi.finstec.finstec.model.ClientsDataSource;
import fi.finstec.finstec.model.DatabaseFactory;
import fi.finstec.finstec.model.EquipmentEntry;
import fi.finstec.finstec.model.EquipmentsDataSource;
import fi.finstec.finstec.model.SiteEntry;
import fi.finstec.finstec.model.SitesDataSource;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
@RunWith(RobolectricTestRunner.class)
public class DatabaseUnitTest {
    @Test
    public void testClients() throws Exception {
        Context context = RuntimeEnvironment.application.getApplicationContext();
        assertNotNull(context);

        DatabaseFactory databaseFactory = DatabaseFactory.getInstance(context);
        assertNotNull(databaseFactory);

        ClientsDataSource clientsDataSource = databaseFactory.getClientsDataSource();
        assertNotNull(clientsDataSource);

        // Database is empty
        List<ClientEntry> clientEntryList = clientsDataSource.getAllClients();
        assertEquals(0, clientEntryList.size());

        // Add a client
        ClientEntry clientEntry1 = new ClientEntry();
        clientEntry1.setName("First client name");

        clientEntry1 = clientsDataSource.addClient(clientEntry1);
        assertNotNull(clientEntry1);

        // Check if the database has one item
        clientEntryList = clientsDataSource.getAllClients();
        assertEquals(1, clientEntryList.size());

        // Add a second item
        ClientEntry clientEntry2 = new ClientEntry();
        clientEntry2.setName("Second client name");
        clientEntry2 = clientsDataSource.addClient(clientEntry2);
        assertNotNull(clientEntry2);

        // Check if database has two items
        clientEntryList = clientsDataSource.getAllClients();
        assertEquals(2, clientEntryList.size());

        // Print the two items
        for (int i = 0; i < clientEntryList.size(); i++) {
            System.out.println("client name: " + clientEntryList.get(i).getName() + " id: " + clientEntryList.get(i).getDatabaseId());
        }



        // Make sure there are no sites
        SitesDataSource sitesDataSource = databaseFactory.getSitesDataSource();
        assertNotNull(sitesDataSource);

        List<SiteEntry> sites = sitesDataSource.getAllSiteEntries();
        assertEquals(0, sites.size());

        // Add first site
        SiteEntry siteEntry1 = new SiteEntry();
        siteEntry1.setName("First site");
        siteEntry1.setClient(clientEntry1);
        siteEntry1 = sitesDataSource.addSite(siteEntry1);
        assertNotNull(siteEntry1);

        sites = sitesDataSource.getAllSiteEntries();
        assertEquals(1, sites.size());

        // Add second site
        SiteEntry siteEntry2 = new SiteEntry();
        siteEntry2.setName("Second site");
        siteEntry2.setClient(clientEntry2);
        siteEntry2 = sitesDataSource.addSite(siteEntry2);
        assertNotNull(siteEntry2);

        sites = sitesDataSource.getAllSiteEntries();
        assertEquals(2, sites.size());

        // Add third site
        SiteEntry siteEntry3 = new SiteEntry();
        siteEntry3.setName("Third site");
        siteEntry3.setClient(clientEntry2);
        siteEntry3 = sitesDataSource.addSite(siteEntry3);
        assertNotNull(siteEntry3);

        sites = sitesDataSource.getAllSiteEntries();
        assertEquals(3, sites.size());

        for (int i = 0; i < sites.size(); i++) {
            SiteEntry siteEntry = sites.get(i);
            System.out.println("Site name: " + siteEntry.getName() + " id: " + siteEntry.getDatabaseId() + " client name: " + siteEntry.getClient().getName());
        }

        // Check if sites with client works
        List<SiteEntry> filteredList = sitesDataSource.getSitesByClient(clientEntry1);
        assertEquals(1, filteredList.size());

        filteredList = sitesDataSource.getSitesByClient(clientEntry2);
        assertEquals(2, filteredList.size());


        // Make sure there are no equipments
        EquipmentsDataSource equipmentsDataSource = databaseFactory.getEquipmentsDataSource();
        assertNotNull(equipmentsDataSource);

        List<EquipmentEntry> equipments = equipmentsDataSource.getAllEquipmentEntries();
        assertEquals(0, equipments.size());

        // Add first equipment
        EquipmentEntry equipmentEntry1 = new EquipmentEntry();
        equipmentEntry1.setSite(siteEntry1);
        equipmentEntry1.setModel("equipment 1");
        equipmentEntry1 = equipmentsDataSource.addEquipment(equipmentEntry1);
        assertNotNull(equipmentEntry1);

        equipments = equipmentsDataSource.getAllEquipmentEntries();
        assertEquals(1, equipments.size());

        // Add second equipment
        EquipmentEntry equipmentEntry2 = new EquipmentEntry();
        equipmentEntry2.setSite(siteEntry2);
        equipmentEntry2.setModel("equipment 2");
        equipmentEntry2 = equipmentsDataSource.addEquipment(equipmentEntry2);
        assertNotNull(equipmentEntry2);

        equipments = equipmentsDataSource.getAllEquipmentEntries();
        assertEquals(2, equipments.size());

        // Add third equipment on site 2
        EquipmentEntry equipmentEntry3 = new EquipmentEntry();
        equipmentEntry3.setSite(siteEntry2);
        equipmentEntry3.setModel("equipment 3");
        equipmentEntry3 = equipmentsDataSource.addEquipment(equipmentEntry3);
        assertNotNull(equipmentEntry3);

        equipments = equipmentsDataSource.getAllEquipmentEntries();
        assertEquals(3, equipments.size());

        for (int i = 0; i < equipments.size(); i++) {
            EquipmentEntry equipmentEntry = equipments.get(i);
            System.out.println("Equipment name: " + equipmentEntry.getModel() + " id: " + equipmentEntry.getDatabaseId() + " site name: " + equipmentEntry.getSite().getName());
        }

        List<EquipmentEntry> filteredEquipmentList = equipmentsDataSource.getEquipmentsBySite(siteEntry1);
        assertEquals(1, filteredEquipmentList.size());

        filteredEquipmentList = equipmentsDataSource.getEquipmentsBySite(siteEntry2);
        assertEquals(2, filteredEquipmentList.size());
    }
}